package karatsuba;

import java.math.BigInteger;
import java.util.Random;

public class Karatsuba
{
	public static BigInteger multiplicar(BigInteger x, BigInteger y)
	{
		int m = Math.max(x.bitLength(), y.bitLength()) / 2;
		
		if( m <= 10 )
			return x.multiply(y);
		
		BigInteger x1 = x.shiftRight(m);
		BigInteger y1 = y.shiftRight(m);
		BigInteger x2 = x.subtract( x1.shiftLeft(m) );
		BigInteger y2 = y.subtract( y1.shiftLeft(m) );
		
		BigInteger A = multiplicar(x1, y1);
		BigInteger B = multiplicar(x2, y2);
		BigInteger C = multiplicar(x1.add(x2), y1.add(y2));
		BigInteger K = C.subtract( A.add(B) );
		
		return A.shiftLeft(2*m).add( K.shiftLeft(m).add(B) );
	}
	
	public static void main(String[] args)
	{
		Random random = new Random(0);
		BigInteger x = new BigInteger(400, random);
		BigInteger y = new BigInteger(400, random);

		BigInteger z1 = x.multiply(y);
		BigInteger z2 = multiplicar(x, y);

		System.out.println(z1);
		System.out.println(z2);
	}	
}
